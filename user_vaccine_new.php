<?php

session_start();

require('inc/pdo.php');
require ('inc/fonction.php');
require ('inc/validation.php');

if(!isLogged()){
    header('location: index.php');
}

$errors = [];
$success = false;

$sql = "SELECT id, title FROM vaccine";
$query = $pdo->prepare($sql);
$query->execute();
$ids = $query->fetchAll();

if (!empty($_POST['submit'])) {
    $id_user = $_SESSION['user']['id'];
    $comment = cleanXss('comment');
    $date = cleanXss('date');
    $rappel = cleanXss('rappel');
    $id_vaccin = cleanXss('list');


    $errors = validText($errors, $comment, 'comment', 2, 250);

    if (empty($_POST['date'])) {
        $errors['date'] = 'Veuillez renseigner une date';
    }

    if (count($errors) == 0) {
        $success = true;

        $sql = "INSERT INTO user_vaccin(id_user,
                                        id_vaccin,
                                        created_at,
                                        vaccine_at,
                                        rappel_at,
                                        comment)
                VALUES (:id_user,
                        :id_vaccin,
                        NOW(),
                        :date,
                        :rappel,
                        :comment)";

        $query = $pdo->prepare($sql);
        $query->bindValue(':id_user', $id_user, PDO::PARAM_INT);
        $query->bindValue(':id_vaccin', $id_vaccin, PDO::PARAM_INT);
        $query->bindValue(':comment', $comment, PDO::PARAM_STR);
        $query->bindValue(':date', $date, PDO::PARAM_STR);
        $query->bindValue(':rappel', $rappel, PDO::PARAM_STR);
        $query->execute();
        $newId = $pdo->lastInsertId();
        header('location:users.php');
    }
}

include('inc/header.php'); ?>

    <section id="addVaccinUser">

        <div class="title">
            <h1>Profil Utilisateur</h1>
        </div>

        <div class="wrapForm">

            <div class="imgForm">
<!--                <img src="divers/image2.png" alt="">-->
            </div>

            <div class="form">

                <form action="" method="post" novalidate>

                    <div class="formInput">
                        <label for="list">Nom du vaccin :</label>
                        <select name="list" id="list" required>
                            <option value="">Selectionner un vaccin</option>
                            <?php foreach ($ids as $id) { ?>
                                <option value="<?php echo $id['id']; ?>"<?php
                                if (!empty($_POST['list']) && $_POST['list'] == $id['id']) { echo ' selected'; }
                                ?>><?php echo $id['title']; ?></option>
                            <?php } ?>
                        </select>
                        <span class="error"><?php if (isset($errors['list'])) { echo htmlspecialchars($errors['list']); } ?></span>
                    </div>

                    <div class="formInput">
                        <label for="date">Date de vaccination :</label>
                        <input type="date" name="date" id="date" value="<?php getPostValue('date'); ?>">
                        <span class="error"><?php viewError($errors, 'date'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="rappel">Date de rappel :</label>
                        <input type="date" name="rappel" id="rappel" value="<?php getPostValue('rappel'); ?>">
                        <span class="error"><?php viewError($errors, 'rappel'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="comment">Commentaire :</label>
                        <textarea name="comment" id="comment" cols="30" rows="3" placeholder="Ex: numéro de lot de votre vaccin / 250 caractère max"></textarea>
                        <span class="error"><?php viewError($errors, 'comment'); ?></span>
                    </div>

                    <div class="formInput">
                        <div class="formButton" >
                            <input type="submit" id="submit" name="submit" value="Ajouter mon vaccin">
                        </div>
                    </div>

                </form>

            </div>

        </div>

    </section>

<?php include('inc/footer.php');
