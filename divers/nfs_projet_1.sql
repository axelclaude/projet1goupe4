-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 15 nov. 2023 à 09:28
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `nfs_projet_1`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `last_name` varchar(120) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `numero_secu` varchar(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  `role` varchar(15) NOT NULL,
  `token` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `last_name`, `first_name`, `password`, `email`, `numero_secu`, `created_at`, `modified_at`, `role`, `token`) VALUES
(1, '', '', '', '', '0', NULL, NULL, 'user', 0),
(2, 'Carl', 'Awel', '$2y$10$26mSuRu9Sam3fFDC45bIsehGYT.k.Miz.d0wAGhplI7dOwDAyc39O', 'axelclauCCCCCde@gmail.com', '19003763452353245', NULL, NULL, 'user', 0),
(3, 'Car', 'Awel', '', 'axelclauCCCCCde@gmail.com', '345', NULL, NULL, '', 0),
(4, 'Carl', 'Awel', '', 'axelclauCCCCCde@gmail.com', '1234', NULL, NULL, '', 0),
(5, 'Carl', 'Awel', '', 'axelclauCCCCCde@gmail.com', '12345678', NULL, NULL, '', 0),
(6, 'Carl', 'Cowsky', '$2y$10$QOOtFpIvf1SW5GPQmlv0suGsX4tFpJ9bgX43Kw19arpdxcupIdF7S', 'ccv@d.fr', '2904395456543', NULL, '2023-11-13 11:51:01', 'user', 9),
(7, 'Carl', 'Cowsky', '$2y$10$YQ3ujrpfh9/1xJ5faeZeEuz012jEGtTNRO7uLGj.He95N0n.dNsoS', 'nouvelessai@gmail.com', '123453234', '2023-11-12 23:57:49', '2023-11-13 00:02:17', 'admin', 0),
(8, 'Claude', 'Axel', '$2y$10$q27TcS/9ZhYd1zLRLpUEGeH.eS220OqsBcQf8Ukr9v3FZBBT9KCK6', 'axelclaude@gmail.com', '1900376456543', '2023-11-13 15:52:39', '2023-11-14 11:17:31', 'admin', 0);

-- --------------------------------------------------------

--
-- Structure de la table `user_vaccin`
--

CREATE TABLE `user_vaccin` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_vaccin` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `vaccine_at` datetime NOT NULL,
  `rappel_at` datetime NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `vaccine`
--

CREATE TABLE `vaccine` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_vaccin`
--
ALTER TABLE `user_vaccin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `user_vaccin`
--
ALTER TABLE `user_vaccin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
