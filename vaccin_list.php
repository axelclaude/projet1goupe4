<?php

session_start();
require('inc/pdo.php');
require('inc/fonction.php');
require('inc/validation.php');

$vaccines = [];
//debug();

if(!empty($_POST['submitted'])) {
    $id_vaccin = cleanXss('id');
    $title = cleanXss('title');
    $description = cleanXss('description');

    if ((!empty($vaccines) and (empty($errors['vaccines'])))) {
        $sql = "SELECT id, title, description, created_at, modified_at, created_by, modifiad_by FROM vaccine ORDER BY id ASC";
        $query = $pdo->prepare($sql);
        $query->execute();
        $vaccines = $query->fetchAll(PDO::FETCH_ASSOC);
    }
}


include('inc/header.php'); ?>

    <section id="list">
        <h1>Liste des Vaccins</h1>
        <ul>
            <?php foreach ($vaccines as $vaccine) { ?>
                <li>
                    <a href="vaccin_list.php?id=<?php echo $vaccine['id']; ?>">
                        <?php echo $vaccine['title']; ?>
                    </a>

                    <form action="supp_vaccin.php" method="post" >
                        <input type="btn" name="btn" value="<?php echo $vaccine['id']; ?>">
                        <button type="submit">Supprimer</button>
                    </form>
                </li>
            <?php } ?>
        </ul>

    </section>
    <a href="user_vaccine_new.php" class="ajout_button">Ajouter</a>

<?php include('inc/footer.php');
