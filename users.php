<?php
session_start();

require('inc/pdo.php');
require('inc/fonction.php');
require('inc/request.php');
require('inc/validation.php');

if(!isLogged()){
    header('location: index.php');
}

$userID = $_SESSION['user']['id'];
$userData = getUserById($userID);

$sql="  SELECT u.*, uv.*, v.* 
        FROM users u, user_vaccin uv, vaccine v 
        WHERE uv.id_user = u.id 
        AND uv.id_vaccin = v.id";
$query = $pdo->prepare($sql);
$query->execute();
$vaccines = $query->fetchAll(PDO::FETCH_ASSOC);

$query = $pdo->prepare($sql);
$query->execute();
$vaccines = $query->fetchAll();


if (!empty($_POST['modified'])) {
    header('Location: user_modif.php?id='.$userData['id'].'');
}

if (!empty($_POST['addVacc'])) {
    header('Location: user_vaccine_new.php');
}

include('inc/header.php');
?>

<section id="profilUser">

    <div class="title">
        <h1>Profil Utilisateur</h1>
    </div>

    <div class="wrapFormList">

        <div class="form">
            <form method="POST" action="users.php">

                <div class="formInput">
                    <label for="last_name">Nom</label>
                    <input type="text" id="last_name" name="last_name" value="<?php echo $userData['last_name']; ?>" disabled>
                </div>

                <div class="formInput">
                    <label for="prenom">Prénom</label>
                    <input type="text" name="prenom" value="<?php echo $userData['first_name']; ?>" disabled>
                </div>

                <div class="formInput">
                    <label for="numero_secu">Numéro de sécurité sociale </label>
                    <input type="text" name="numero_secu" value="<?php echo $userData['numero_secu']; ?>" disabled>
                </div>

                <div class="formInput">
                    <label for="email">Email </label>
                    <input type="email" name="email" value="<?php echo $userData['email']; ?>" disabled>
                </div>

                <div class="formInput">
                    <div class="formButton">
                        <input type="submit" name="modified" value="Modifier">
                    </div>
                </div>

            </form>
        </div>

        <div class="form">
            <form method="POST" action="">
                <?php  if(!empty($vaccines))
                {foreach ($vaccines as $vaccine) { ?>

                <div class="formInput">
                    <label for="Vaccin"><?php echo $vaccine['title']; ?> fait le <?php echo date('d/m/Y',strtotime($vaccine['vaccine_at'])); ?></label>
                    <input type="text" name="Vaccin" value="<?php echo $vaccine['comment']; ?>" disabled>
                    <input type="text" name="Vaccin" value="<?php echo 'Rappel avant le: '.date('d/m/Y',strtotime($vaccine['rappel_at'])); ?>" disabled>
                </div>

                <?php }} ?>

                <div class="formInput">
                    <div class="formButton">
                        <input type="submit" name="addVacc" value="Ajouter">
                    </div>
                </div>
            </form>
        </div>

    </div>
</section>



<?php

include('inc/footer.php');

