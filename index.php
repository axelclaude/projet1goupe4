<?php

session_start();
require ('inc/fonction.php');
require ('inc/pdo.php');
require ('inc/request.php');


include ('inc/header.php');?>
    <section id="intro">
        <div class="wrap">
            <div class="medecin">
                <div class="vax">
                    <img src="divers/clara_1.png" width= "5%" alt="medecin">
                </div>
                <div class="savoir">
                    <li class="more"><a href="#read">INSCRIPTION</a></li>
                </div>
            </div>
        </div>
    </section>
    <section id="bleu">
        <div class="wrap2">
            <div class="carre">
                <div class="bleu">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis ducimus eius in ipsa nesciunt quae quod recusandae, sequi veniam.</p>
                </div>
                <div class="bleu">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis ducimus eius in ipsa nesciunt quae quod recusandae, sequi veniam.</p>
                </div>
                <div class="bleu">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A corporis ducimus eius in ipsa nesciunt quae quod recusandae, sequi veniam.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="diapo">
        <div class="wrap2">
            <div class="imgdiapo">
                <img src="divers/pexels-rfstudio-3825529.jpg" alt="vaccin">
            </div>
            <div class="text">
                <p>Le vaccin, un pilier de la santé publique, est un outil médical révolutionnaire qui a transformé la lutte contre de nombreuses maladies infectieuses. Il fonctionne en préparant le système immunitaire à reconnaître et combattre les agents pathogènes, sans exposer la personne à la maladie elle-même.

                    Premièrement, les vaccins sauvent des vies. Des maladies autrefois mortelles comme la variole ont été éradiquées grâce à la vaccination. La polio, qui causait paralysie et handicap, est désormais rare dans de nombreuses régions du monde, grâce à des programmes de vaccination intensifs.</p>
            </div>
        </div>
    </section>
    <section id="milieux">
        <div class="wrap2">
            <div class="titre">
                <div class="titrebas">
                    <h3>Autres articles</h3>
                </div>
                <div class="boites">
                    <div class="boite1">
                        <img src="divers/image_1_bas.png" alt="">
                        <h2>Mondial</h2>
                        <p>Campagnes de Vaccination à Travers le Monde.</p>
                    </div>
                    <div class="boite2">
                        <img src="divers/image_2_bas.png" alt="">
                        <h2>Ecoles</h2>
                        <p>Campagnes de Vaccination dans les écoles.</p>
                    </div>
                    <div class="boite3">
                        <img src="divers/image_3_bas.png" alt="">
                        <h2>Entreprises</h2>
                        <p>Campagnes de Vaccination dans les entreprises.</p>
                    </div>
                </div>
            </div>
            <div class="about">
                <li class="us"><a href="">ABOUT</a></li>
            </div>
            <div class="lore1">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam earum minima possimus qui reprehenderit voluptates?</p>
            </div>
        </div>
    </section>
<?php include ('inc/footer.php');