<?php
//require('fonction.php');
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo pathinfo(basename($_SERVER['PHP_SELF']), PATHINFO_FILENAME); ?></title>
    <meta name="description" content="Max 156 caractères, visible sur la description du site (SEO)">
    <meta name="keywords" content="mots clé, séparés, par, virgules ">
    <meta name="author" content="Claude Axel">
    <meta name="robots" content="follow, index">
    <link rel="shortcut icon" href="../asset/img/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="asset/css/style.css">
</head>
<body>
<section id="masterhead">
    <div class="wrap">
        <div class="logohead">
            <nav>
                <ul>
                    <li><a href="index.php"><img src="divers/logo_header.png" alt=""></a></li>
                </ul>
        </div>
        <div class="phrase">
            <h1>Merci qui ? Merci VaxHub !</h1>
        </div>
        <div class="lienhead">
            <nav>
                <ul class="link">
                    <?php if(isLogged()) { ?>
                        <button id="myBtn"><i class="fa-solid fa-square-plus"></i>URGENCES</button>
                        <div id="myModal" class="modal">
                            <div class="modal-content">
                                <span class="close">&times;</span>
                                <p>Numéros d'urgences : en cas de besoin, contactez </p>
                                <ul>
                                    <li><i class="fa-solid fa-phone-flip" style="color: #090a0b;"></i> Pompiers : 18</li>
                                    <li><i class="fa-solid fa-phone-flip" style="color: #090a0b;"></i> Samu : 15</li>
                                </ul>
                            </div>
                        </div>
                        <li class="register"><a href="logout.php">LOGOUT</a></li>
                        <li class="espace"><a href="users.php">MON PROFIL</a></li>
                    <?php } else { ?>
                        <button id="myBtn"><i class="fa-solid fa-square-plus"></i>URGENCES</button>
                        <div id="myModal" class="modal">
                            <div class="modal-content">
                                <span class="close">&times;</span>
                                <p>Numéros d'urgences : en cas de besoin, contactez </p>
                                <ul>
                                    <li><i class="fa-solid fa-phone-flip" style="color: #090a0b;"></i> Pompiers : 18</li>
                                    <li><i class="fa-solid fa-phone-flip" style="color: #090a0b;"></i> Samu : 15</li>
                                </ul>
                            </div>
                        </div>
                        <li class="register"><a href="register.php">INSCRIPTION</a></li>
                        <li class="espace"><a href="login.php">MON ESPACE</a></li>
                    <?php } ?>
                </ul>
            </nav>
            <?php if(isAdmin()) { ?>
                <nav>
                    <form action="">
                        <ul>
                            <li class="admin"><a href="admin/index.php">ADMIN</a></li>
                        </ul>
                    </form>
                </nav>
            <?php } ?>
        </div>
    </div>
    <div class="bandeau">
    </div>
</section>
<script>

    var modal = document.getElementById("myModal");

    var btn = document.getElementById("myBtn");

    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
