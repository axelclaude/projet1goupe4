<?php


function viewError($err, $key)
{
    if(!empty($err[$key])) {
        echo $err[$key];
    }
}
function validSelect($err, $value, $keyErr, $allowed_values) {
    if (!empty($value)) {
        if (!in_array($value, $allowed_values)) {
            $err[$keyErr] = 'Valeur non valide';
        }
    } else {
        $err[$keyErr] = 'Veuillez sélectionner une option';
    }
    return $err;
}

function validEmail($errors, $data, $key) {
    if (empty($data)) {
        $errors[$key] = "Le champ '$key' est obligatoire.";
    } elseif (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
        $errors[$key] = "Le format de l'email est invalide.";
    }
    return $errors;
}

function validPassword($errors, $password, $password2, $key) {
    if (empty($password)) {
        $errors[$key] = "Le mot de passe est obligatoire.";
    } elseif (strlen($password) < 6) {
        $errors[$key] = "Le mot de passe doit contenir au moins 6 caractères.";
    }
    if ($password !== $password2) {
        $errors[$key . '2'] = "Les mots de passe ne correspondent pas.";
    }
    return $errors;
}



function validText($err, $value, $keyErr, $min, $max) {
    if (!empty($value)) {
        if (mb_strlen($value) < $min) {
            $err[$keyErr] = 'Veuillez renseigner au moins '.$min.' caractères';
        } elseif (mb_strlen($value) > $max) {
            $err[$keyErr] = 'Veuillez renseigner pas plus de '.$max.' caractères';
        }
    } else {
        $err[$keyErr] = 'Veuillez renseigner ce champ';
    }
    return $err;
}


