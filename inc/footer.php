<footer id="footer">
    <div class="chevron">
        <ul>
            <li><a href="#masterhead"><i class="fa-solid fa-chevron-up"></i></a></li>
        </ul>
    </div>
    <div class="wrap">
        <div class="logo">
            <ul>
                <li><a href="#masterhead"><img src="divers/logo_header.png" alt=""></a></li>
            </ul>
        </div>
        <div class="lienfooter">
            <div>
                <ul>
                    <li><a href="contact.php">CONTACT</a></li>
                    <li><a href="faq.php">FAQ</a></li>
                    <li><a href="mentions.php">MENTIONS LEGALES</a></li>
                    <li><a href="cgu.php">CGU</a></li>
                </ul>
            </div>
        </div>
        <div class="suiveznous">
                <p>CONTACTEZ-NOUS</p>
                <ul class="contact">
                    <a href="https://twitter.com/home" target="_blank"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.facebook.com/login/native_sso?app_id=1931350367173590&token=AbhMp5EOujrJlj9Q-ZnOHmjHWzxp0Gojcu8EFRjBsS88I1RAr11DJTyJliRu9CNn4pEQhkDIzAqb8Q#_=_" target="_blank"><i class="fa-brands fa-facebook"></i></a>
                    <a href="""><i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fa-brands fa-facebook"></i></a>
                    <a href=""><i class="fa-regular fa-envelope"></i></a>
                </ul>
        </div>
    </div>
    <div class="wrapfooter">
        <div class="copyright">
            <p><i class="fa-regular fa-copyright"></i>Copyright 2023 VaxHub All Rights Reserved</p>
        </div>
    </div>
</footer>
</body>
</html>