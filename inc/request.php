<?php

function getAllVaccine() {
    global $pdo;
    $sql = "SELECT * FROM vaccine";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function insertVaccine($title, $description, $created_at, $created_by) {
    global $pdo;
    $sql = "INSERT INTO vaccine (title, description, created_at, created_by) VALUES (:title, :description, :created_at, :created_by)";
    $query = $pdo->prepare($sql);
    $query->bindValue(':title', $title, PDO::PARAM_STR);
    $query->bindValue(':description', $description, PDO::PARAM_STR);
    $query->bindValue(':created_at', $created_at, PDO::PARAM_STR);
    $query->bindValue(':created_by', $created_by, PDO::PARAM_INT);
    $query->execute();
}


function getVaccineById($id) {
    global $pdo;
    $sql = "SELECT * FROM vaccine WHERE id = :id";

    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

function updateVaccine($id, $title, $description, $modified_at, $modified_by) {
    global $pdo;
    $sql = "UPDATE vaccine SET title = :title, description = :description, modified_at = :modified_at, modified_by = :modified_by WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->bindValue(':title', $title, PDO::PARAM_STR);
    $query->bindValue(':description', $description, PDO::PARAM_STR);
    $query->bindValue(':modified_at', $modified_at, PDO::PARAM_STR);
    $query->bindValue(':modified_by', $modified_by, PDO::PARAM_INT);
    $query->execute();
}

function getAllVaccinEnCours() {
    global $pdo;
    $sql = "SELECT S.*, U.name, V.title
            FROM user_vaccin S
            INNER JOIN user U ON S.id_user = U.id
            INNER JOIN vaccin V ON S.id_vaccin = V.id";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getUserById($id) {
    global $pdo;
    $sql = "SELECT * FROM users WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}
function updateUser($id, $lastName, $firstName, $email, $numeroSecu)
{
    global $pdo;
    $genre = getGenderBySecuNumber($numeroSecu);
    $sql = "UPDATE users SET last_name = :last_name, first_name = :first_name, email = :email, numero_secu = :numero_secu, modified_at = NOW() WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue(':last_name', $lastName, PDO::PARAM_STR);
    $query->bindValue(':first_name', $firstName, PDO::PARAM_STR);
    $query->bindValue(':email', $email, PDO::PARAM_STR);
    $query->bindValue(':numero_secu', $numeroSecu, PDO::PARAM_STR);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    return $genre;
}



function getAllUser() {
    global $pdo;
    $sql = "SELECT * FROM users";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function insertVaccinEncoursToDo($date_todo, $vaccin_id, $user_id)
{
    global $pdo;
//    $allvaccinencours = getAllVaccinEnCours();
//    $userconnected = $_SESSION['verifLogin']['id'];
//
//    foreach ($allvaccinencours as $allvaccinencour) {
//        $allvaccin = $allvaccinencour['vaccin_id'];
//    }
    $sql="SELECT * FROM vaccin_status WHERE vaccin_id=:idva AND user_id = :id";
    $query= $pdo->prepare($sql);
    $query->bindValue('idva', $vaccin_id);
    $query->bindValue('id', $user_id);
    $query->execute();
    $verifId= $query->fetch();

    if (!empty($verifId)) {

        $sql="UPDATE vaccin_status 
                SET date_todo= :date , status='en attente'
                WHERE vaccin_id = :idva AND
                user_id = :id";
        $query = $pdo->prepare($sql);
        $query->bindValue('date', $date_todo, PDO::PARAM_STR);
        $query->bindValue('idva', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('id', $user_id, PDO::PARAM_STR);
        $query->execute();
        return $pdo->lastInsertId();

    } else {
        global $pdo;
        $dose=0;

        $sql = "INSERT INTO vaccin_status (vaccine_at, doses, vaccin_id, user_id, status, created_at) 
                VALUES (:vaccine_at, :doses, :vaccin_id,:user_id, 'en attente', NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('date_todo', $date_todo, PDO::PARAM_STR);
        $query->bindValue('doses', $dose, PDO::PARAM_STR);
        $query->bindValue('vaccin_id', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
        return $pdo->lastInsertId();
    }
}

