<?php

function debug($array){
    echo '<pre>';
    echo print_r($array);
    echo '</pre>';
}

function cleanXss($key)
{
    if (isset($_POST[$key])) {
        return trim(strip_tags($_POST[$key]));
    } else {
        return '';
    }
}


function random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $characters_length = strlen($characters);
    $random_string = '';
    for ($i = 0; $i < $length; $i++) {
        $random_string .= $characters[rand(0, $characters_length - 1)];
    }
    return $random_string;
}


function getPostValue($key){
    if (!empty($_POST[$key])) {
        echo $_POST[$key];
    }
    return $key;
}

function isLogged()
{
    if(!empty($_SESSION['user']['id'])) {
        if(!empty($_SESSION['user']['email'])) {
            if(!empty($_SESSION['user']['role'])) {
                if (!empty($_SESSION['user']['ip'])) {
                    if ($_SESSION['user']['ip'] == $_SERVER['REMOTE_ADDR']) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

function isAdmin() {
    if(isLogged()) {
        if($_SESSION['user']['role'] == 'admin') {
            return true;
        }
    }
    return false;
}

function getInfoById($id) {
    global $pdo;
    $sql = "SELECT * FROM rappel WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}
function getGenderBySecuNumber($numeroSecu) {
    $numeroSecuStr = strval($numeroSecu);
    $dernierChiffre = $numeroSecuStr[strlen($numeroSecuStr) - 1];
    $genre = ($dernierChiffre % 2 == 0) ? 'Femme' : 'Homme';
    return $genre;
}
function infoBySecuNum($numeroSecu){
    $gender = substr($numeroSecu, 0, 1);
    $dpt = substr($numeroSecu, 5, 2);
    $year = substr($numeroSecu, 1, 2);
    if($year > substr(date('Y'), -2)){
        $firstYear = 19;
    } else {
        $firstYear = 20;
    }
    $userGender = ($gender == 1) ? 'Homme' : 'Femme';
    $firstYear .= $year;
    return array('gender' => $userGender, 'dpt' => $dpt, 'year' => $firstYear);
}

function getAgeBySecuNumber($numeroSecu) {
    $numeroSecu = strval($numeroSecu);
    if (strlen($numeroSecu) !== 13 && strlen($numeroSecu) !== 15) {
        return false;
    }

    $sexeEtSiecle = substr($numeroSecu, 0, 1);
    $annee = substr($numeroSecu, 1, 2);
    $mois = substr($numeroSecu, 3, 2);

    if ($sexeEtSiecle == '1' || $sexeEtSiecle == '2') {
        $century = '19';
    } elseif ($sexeEtSiecle == '3' || $sexeEtSiecle == '4') {
        $century = '20';
    } else {
        return false;
    }

    $anneeComplete = $century . $annee;

    $dateNaissance = DateTime::createFromFormat('Y-m', $anneeComplete . '-' . $mois);
    if (!$dateNaissance) {
        return false; // La date de naissance n'est pas valide.
    }

    $dateActuelle = new DateTime();
    if ($dateNaissance > $dateActuelle) {
        $dateNaissance->sub(new DateInterval('P100Y'));
    }

    $age = $dateActuelle->diff($dateNaissance)->y;

    return $age;
}
