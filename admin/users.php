<?php
session_start();

require ('../inc/pdo.php');
require ('../inc/fonction.php');
require ('../inc/validation.php');
require ('../inc/request.php');

//if (isLogged()) {
//    if ($_SESSION['verifLogin']['role'] == 'admin') {
//
//    }
//} else {
//    header('Location: 404.php');
//}

$users = getAllUser();

?>
<?php include ('inc/header.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <h1 class="h3 mb-2 text-gray-800">Gestion Utilisateur</h1>
                    <p class="mb-4">Cette table vous permet de gerer les utilisateurs et de les modifier ou les bannir.</p>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 style="padding-bottom: 1rem" class="m-0 font-weight-bold text-primary">Utilisateurs</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>Email</th>
                                        <th>sécu</th>
                                        <th>Sexe</th>
                                        <th>année de naissance</th>
                                        <th>Département de naissance</th>
                                        <th>role</th>
                                        <th>Date de création</th>
                                        <th>Dernière modification</th>
                                        <th>Modifier/Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>Email</th>
                                        <th>sécu</th>
                                        <th>role</th>
                                        <th>Date de création</th>
                                        <th>Dernière modification</th>
                                        <th>Modifier/Supprimer</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php foreach ($users as $user) {
                                        $userSecu = infoBySecuNum($user['numero_secu']);
                                        ?>

                                        <tr>
                                            <td><?= $user['last_name'] ?></td>
                                            <td><?= $user['first_name'] ?></td>
                                            <td><?= $user['email'] ?></td>
                                            <td><?= $user['numero_secu'] ?></td>
                                            <td><?= $userSecu['gender'] ?></td>
                                            <td><?= $userSecu['year'] ?></td>
                                            <td><?= $userSecu['dpt']?></td>
                                            <td><?= $user['role'] ?></td>
                                            <td><?= $user['token'] ?></td>
                                            <td><?= $user['created_at'] ?></td>
                                            <td style="display: flex; justify-content: space-between">
                                                <a title="Editer" style="font-size: 1.5rem" href="edituser.php?id=<?= $user['id']; ?>"><i class="fa-solid fa-pen-to-square"></i></a>
                                                <a title="Administrateur" style="font-size: 1.0rem" href="admin_user.php?id=<?= $user['id']; ?>"><i class="fa-solid fa-user-plus"></i>donner le role admin</a>
<!--                                                <a title="Utilisateur" style="font-size: 1.5rem" href="republishuser.php?id=--><?php //= $user['id']; ?><!--"><i class="fa-solid fa-user"></i></a>-->
                                                <a title="Bannir" style="font-size: 1.0rem" href="delete_user.php?id=<?= $user['id']; ?>"<i class="fa-solid fa-user-xmark">supprimer</i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
<?php include ('inc/footer.php'); ?>
