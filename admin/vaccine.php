<?php

session_start();


require '../inc/pdo.php';
require '../inc/fonction.php';
require '../inc/validation.php';
require '../inc/request.php';

//if (isLogged()) {
//    if ($_SESSION['verifLogin']['role'] == 'admin') {
//
//    }
//} else {
//    header('Location: 404.php');
//}

$vaccins = getAllVaccine();
?>
<?php include ('inc/header.php'); ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Gestion Vaccin</h1>
    <p class="mb-4">Cette table vous permet de gerer les vaccins d'en créer, de les modifier ou les supprimer (Attention! cette action est irreversible).</p>

    <!-- DataTales Example -->
    <div id="tableau" class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 style="padding-bottom: 1rem" class="m-0 font-weight-bold text-primary">Vaccins</h6>
            <a class="btn btn-primary" href="new_vaccine.php">Ajouter un nouveau vaccin</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Date de création</th>
                        <th>Dernière modification</th>
                        <th>Modifier/Supprimer</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Date de création</th>
                        <th>Dernière modification</th>
                        <th>Modifier/Supprimer</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php foreach ($vaccins as $vaccin) { ?>
                        <tr>
                            <td><?= $vaccin['title'] ?></td>
                            <td><?= $vaccin['description'] ?></td>
                            <td><?= $vaccin['created_at'] ?></td>
                            <td><?= $vaccin['modified_at'] ?></td>
                            <td style="display: flex; justify-content: space-between">
                                <a title="Editer" style="font-size: 1rem gap: 1.5rem" href="edit_vaccine.php?id=<?= $vaccin['id']; ?>"><i class="fa-solid fa-pen-to-square"></i>modifier</a>
                                <a title="Supprimer definitivement" style="font-size: 1rem gap: 1.5rem" href="delete_vaccine.php?id=<?= $vaccin['id']; ?>"><i class="fa-solid fa-trash">supprimer</i></a>
                            </td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>
