<?php
session_start();
require ('../inc/pdo.php');
require ('../inc/fonction.php');
require ('../inc/validation.php');
require ('../inc/request.php');

if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
$id = $_GET['id'];
$user = getUserById($id);

if (empty($user)) {
die('404');
} else {
// Récupération du numéro de sécurité sociale depuis la base de données
$numeroSecu = $user['numero_secu'];

// Calcul du genre et de l'âge de l'utilisateur
$genre = getGenderBySecuNumber($numeroSecu);
$age = getAgeBySecuNumber($numeroSecu);

// Vous pouvez maintenant utiliser $genre et $age pour les afficher ou les enregistrer dans la base de données si nécessaire
// Par exemple :
// echo "Le genre est : $genre et l'âge est : $age";

// Mise à jour du rôle de l'utilisateur en 'admin'
$sql = "UPDATE users SET role = 'admin' WHERE id = :id";
$query = $pdo->prepare($sql);
$query->bindValue(':id', $id, PDO::PARAM_INT);
$query->execute();

header('Location: users.php');
exit; // Assurez-vous de terminer le script après une redirection
}
} else {
die('404');
}
