<?php

require_once('../inc/pdo.php');
require_once('../inc/fonction.php');
require_once('../inc/request.php');
require_once('../inc/validation.php');

if(!isAdmin()) {
    header('Location: ../403.php');
}


$sql = "SELECT * FROM `users`";
$query = $pdo -> prepare($sql);
$query -> execute();
$user_stat = $query -> fetchAll();


$sql = "SELECT * FROM `vaccine`";
$query = $pdo -> prepare($sql);
$query -> execute();
$vaccin_stat = $query -> fetchAll();




?>
<!doctype html>
<link rel="stylesheet" href="asset/css/sb-admin.css">
<link rel="stylesheet" href="asset/css/style_back.css">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-LdHvAqAR3L5c8O0wQdtPSDs95SvPCd2/0dw1DdNc4J3gV5U3eOs8wcuE+1bw8cVe2OJU5XvZVtU8a8stPfD3Bw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title>vaxhub admin</title>
</head>
<body>
<section id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../index.php">
                <img src="../divers/logo_header.png" alt="" style="width: 50px">
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item">
                <a class="nav-link" href='index.php'>
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>retour a l'admin du site</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Information Utilisateurs
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="vaccine.php" data-toggle="collapse" data-target="#collapseUtilities"
                   aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fa fa-tools" aria-hidden="true"></i>
                    <span>gestion des vaccins</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="users.php" data-toggle="collapse" data-target="#collapseUtilities"
                   aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fa fa-tools" aria-hidden="true"></i>
                    <span>gestions des users</span>
                </a>
            </li>
        </ul>

