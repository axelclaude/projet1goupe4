<?php

session_start();
//require('../inc/pdo.php');
//require ('../inc/fonction.php');
//require ('../inc/request.php');
//require ('../inc/validation.php');
//
//if(!isAdmin()) {
//    header('Location: ../403.php');
//}
//
//
//$sql = "SELECT * FROM `users`";
//$query = $pdo -> prepare($sql);
//$query -> execute();
//$user_stat = $query -> fetchAll();
//
//
//$sql = "SELECT * FROM `vaccine`";
//$query = $pdo -> prepare($sql);
//$query -> execute();
//$vaccin_stat = $query -> fetchAll();
//


include ('inc/header.php') ?>

    <section id="page-top">
        <div id="wrapper">

            <div id="content-wrapper" class="d-flex flex-column">
                <div id="content">
                    <!--                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">-->
                    <!--                        --><?php //if ($nbr_msg > 0){ ?>
                    <!--                            <a href="message.php" style="margin-right: .5rem">Message non lu</a>-->
                    <!--                            <span style="background-color: red; height: 20px; width: 20px; border-radius: 50%; display: flex; align-items: center; justify-content: center; color: white;}">--><?php //echo $nbr_msg; ?><!--</span>-->
                    <!--                        --><?php //} ?>
                    <!--                    </nav>-->
                    <div class="container-fluid">
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800">admin-vaxhub</h1>
                        </div>
                        <div class="row">
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-primary shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    nombre utilisateur total</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($user_stat)?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-success shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    nombre de vaccin total</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo count($vaccin_stat) ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-4">
                                <div class="card border-left-info shadow h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">

                                                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">pourcentage de rappel des vaccins

                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">80%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-info" role="progressbar"
                                                                 style="width: 80%" aria-valuenow="50" aria-valuemin="0"
                                                                 aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

<?php include ('inc/footer.php');

