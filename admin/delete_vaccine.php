<?php
session_start();

require '../inc/pdo.php';
require '../inc/fonction.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['role'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}



if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $post = getVaccineById($id);
    if(empty($post)) {
        die('404');
    } else {
        $sql = "DELETE FROM vaccine WHERE id = :id";
        $query = $pdo->prepare($sql);
        $query->bindValue('id',$id, PDO::PARAM_INT);
        $query->execute();
        header('Location: vaccine.php#tableau');
    }
} else {
    die('404');
}
