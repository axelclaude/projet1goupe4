<?php
session_start();

require ('../inc/pdo.php');
require ('../inc/fonction.php');
require ('../inc/validation.php');
require ('../inc/request.php');

if (isAdmin()) {
    if ($_SESSION['user']['role'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}

$errors = array();
$success = false;

if (!empty($_POST['submitted'])) {
    $title = cleanXss('title');
    $description = cleanXss('description');

    $errors = validText($errors, $title, 'title', 4, 150);

    $errors = validText($errors, $description, 'description', 1, 500);

    if (count($errors) == 0){
        $created_at = date('Y-m-d H:i:s');
        $created_by = $_SESSION['user']['id'];
        insertVaccine($title,$description  ,$created_at, $created_by);
       $success = true;
        header('Location: vaccine.php'); // Remplacez 'page_de_succes.php' par la page de votre choix
        exit();
   }
}

?>

<?php include ('inc/header.php'); ?>
            <div class="container-fluid">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Ajouter un nouveau vaccin</h1>
                <div class="card shadow mb-4">
                    <form style="margin: 1rem" action="" method="post" novalidate>
                        <div class="label1" style="display: flex; flex-direction: column; padding-bottom: 3rem">
                            <label for="title">Nom du vaccin</label>
                            <input type="text" name="title" placeholder="Mon Vaccin" id="title" value="<?php getPostValue('title'); ?>">
                            <span class="errors"><?php viewError($errors, 'title'); ?></span>
                        </div>

                        <div class="label2" style="display: flex; flex-direction: column; padding-bottom: 02rem">
                            <label for="description">Description du vaccin</label>
                            <textarea style="max-height: 200px; min-height: 150px" name="description" id="description" cols="20" rows="10"><?php getPostValue('description'); ?></textarea>
                            <span class="errors"><?php viewError($errors, 'description'); ?></span>
                        </div>
                        <div style="display: flex; justify-content: center" class="label3">
                            <input type="submit" name="submitted" value="Ajouter un vaccin">
                        </div>
                    </form>
                </div>
            </div>


<?php include ('inc/footer.php'); ?>
