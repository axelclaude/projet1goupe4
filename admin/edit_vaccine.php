<?php
session_start();

require '../inc/pdo.php';
require '../inc/fonction.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isAdmin()) {
    if ($_SESSION['user']['role'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}

$vaccin = null;

if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $vaccin = getVaccineById($id);
    if (!$vaccin) {
        $vaccin = [];
    }
}

$errors = array();
$success = false;

if (!empty($_POST['submitted'])) {
    $title = cleanXss('title');
    $description = cleanXss('description');

    $errors = validText($errors, $title, 'title', 4, 150);
    $errors = validText($errors, $description, 'description', 4, 500);

    if (count($errors) == 0) {
        $modified_at = date('Y-m-d H:i:s'); // Date et heure actuelles
        $modified_by = $_SESSION['user']['id']; // Supposons que l'ID utilisateur est stocké dans la session

        updateVaccine($id, $title, $description, $modified_at, $modified_by);
        $success = true;
        header('Location: vaccine.php#tableau');
        exit;
    }
}
?>

<?php include ('inc/header.php'); ?>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Modifier un vaccin</h1>
    <div class="card shadow mb-4">
        <form style="margin: 1rem" action="" method="post" novalidate>
            <div class="mb-3">
                <label for="title">Nom du vaccin</label>
                <input type="text" name="title" id="title" value="<?php echo isset($vaccin['title']) ? $vaccin['title'] : ''; ?>" class="form-control">
                <span class="error"><?php viewError($errors, 'title'); ?></span>
            </div>
            <div class="mb-3">
                <label for="description">Description du vaccin</label>
                <textarea name="description" id="description" class="form-control" rows="10"><?php echo isset($vaccin['description']) ? $vaccin['description'] : ''; ?></textarea>
                <span class="error"><?php viewError($errors, 'description'); ?></span>
            </div>
            <div class="mb-3">
                <input type="hidden" name="submitted" value="1">
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </form>
    </div>
</div>
<?php include ('inc/footer.php'); ?>
