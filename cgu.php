<?php

session_start();


require('inc/fonction.php');
require('inc/validation.php');
require('inc/request.php');

include('inc/header.php'); ?>

<section id="wrap">
    <header id="header_cgu">
            <div class="cgu_title">
                <h1>Conditions Générales d'utilisation et de services</h1>
                <p>L'accès et l'utilisation du site accessible sous l'adresse vaxhub.fr (ci-après le « Site ») impliquent l'acceptation tacite et sans réserve des présentes Conditions par le Visiteur, sous leur dernière version en ligne.
                    L'« Editeur » sont ici "..";
                    Les « Utilisateur(s) » sont entendus au sens des présentes comme toute personne physique ou morale utilisant le Site.
                    Les « Visiteur(s) » sont entendus au sens des présentes comme toute personne physique ou morale se connectant et naviguant sur le Site.
                    Les « Administrateur(s) » sont entendus au sens des présentes comme toute personne physique, liée à l'entreprise, ayant l'autorité de modifier une partie du contenu du Site.</p>
            </div>
    </header>
    <main id="main_articles">
        <div class="art1">
            <h2>1. Description du site</h2>
            <p> L'Editeur propose sur le Site de l'information répartie de manière thématique et précisément segmentée concernant les collectivités territoriales de FRANCE.

                A l'exception de certaines informations et services proposés aux Utilisateurs dans le cadre d'un appel téléphonique surtaxé, l'ensemble des informations consultables sur le Site sont librement accessibles, sous réserve du respect des présentes Conditions.

                L'Editeur agit en parfaite indépendance à l'égard de l'administration, des collectivités territoriales et de l'ensemble des acteurs locaux (professionnels, institutions, organismes, services publics, …) et sites web référencés sur le Site.
                Il est à ce titre est entièrement libre dans le choix de la sélection et de l'organisation des informations, des fonctionnalités et des services accessibles sur le Site.
                Il se réserve en outre la faculté discrétionnaire, à tout moment et sans préavis, d'éditer et de supprimer tout ou partie de ces informations, ainsi que de modifier suspendre temporairement ou définitivement tout ou partie des fonctionnalités et services proposées sur le Site.

                Le Site et les Services sont en outre fournis à titre privé uniquement, et ne correspondent en aucune manière possible à une mission de service public qui lui aurait été déléguée par une quelconque administration publique ou collectivité territoriale. 5. Pour toute question sur le Site et les activités de l'Editeur, les Utilisateurs sont invités à contacter VAXHUB aux coordonnées figurant dans les mentions légales. 6. Le site dispose d'un annuaire des institutions , des administrations et des organismes conventionnés soit ajouté par ses Administrateurs</p>
        </div>
        <div class="separateur"></div>
        <div class="art2">
            <h2>2. Politique de protection des données personnelles</h2>
            <p>Le Site est engagé dans une démarche continue de conformité avec le Règlement général sur la protection des données du 25 mai 2018. Avec ce nouveau règlement le Site renforce sa politique de protection de données personnelles afin que les données de nos utilisateurs soient collectées et utilisées de manière transparente, confidentielle et sécurisée.</p>
        </div>
        <div class="separateur"></div>
        <div class="art3">
            <h2>3. Clarté, transparence et loyauté</h2>
            <p>Le Site propose un service privé d'aide administrative par téléphone (surtaxé, cf 7. des CGU)
                L'ensemble des conseillers téléphoniques sont salariés de l'entreprise
                Les conseillers téléphoniques sont également rédacteurs des articles du Site.
                Les informations communiquées par les conseillers téléphoniques à l'appelant sont données à titre indicatif. Elles sont issues de leur expérience dans le domaine, et de l'ensemble des procédures administratives fournies par les administrations.
                Le Site utilise la technologie Elasticsearch pour le fonctionnement de son moteur de recherche.

                Les résultats affichés dans la rubrique annuaire du Site sont classés par critères de priorité.
                Le premier critère est la géolocalisation. Pour exemple : en recherchant la CAF de Grenoble, le système retourne la CAF la plus proche de la mairie de Grenoble.
                Le second critère est le rattachement manuel. Pour exemple : si l'un des administrateurs du Site a rattaché un CCAS à une ville, alors ce CCAS prendra le dessus.
                Enfin, le troisième critère est la pertinence. Le système recherche les mots-clés demandés et retourne les résultats associés et pertinents.

                L'annuaire du Site ne fonctionne pas sous rémunération. Il n'existe aucun moyen financier pour qu'une annonce soit "boostée", et ainsi être priorisée par rapport à une autre.
                Les articles du Site sont rédigés par l'équipe rédactionnelle.
                Certains articles du Site sont issus de dépêches de l'Agence France Presse.
                Certains articles peuvent être rémunérés par le biais de lien vers le site internet d'une entreprise externe. Néanmoins, le Site ne monétise pas plus de 2% de ses articles.
            </p>
        </div>
        <div class="separateur"></div>
        <div class="art4">
            <h2>4. Accès et disponibilité</h2>
            <p>Pour accéder à certaines informations, et en particulier pour être mis en relation avec un conseiller, l'Utilisateur aura la faculté d'effectuer un appel téléphonique surtaxé, conformément au mode opératoire suivant :
                - Composer les numéros de téléphone indiqués dans la rubrique et sur la page web concernée (0.80 euro TTC par minute ou 2.99 euro TTC par appel) ;
                Les montants correspondant à l'accès aux informations seront directement imputés sur la facture de l'opérateur téléphonique de l'Utilisateur. Ils ne comprennent pas les coûts liés au matériel informatique et à la connexion de l'Utilisateur à l'Internet, ainsi que les coûts des appels passés depuis un téléphone mobile, chacun de ces coûts demeurant à la charge de l'Utilisateur.
                Le moyen de paiement par appel surtaxé ne permet pas à VAXHUB de délivrer une facture à l'Utilisateur.

                Les services d'appel surtaxé donnant lieu à une prestation immédiate par un conseiller, l'Utilisateur ne pourra exercer de droit de rétractation concernant ces services, dans la mesure où, conformément aux dispositions de l'article L.121-21-8 1°) du Code de la Consommation, l'exécution de ces services aura commencé avec son accord et aura été réalisée avant la fin du délai de rétractation de quatorze (14) jours ouvrés.

                En cas de difficultés dans l'utilisation de ces services payant, les Utilisateurs sont invités à contacter VAXHUB aux coordonnées figurant en tête des présentes.
            </p>
        </div>
        <div class="separateur"></div>
        <div class="art5">
            <h2>5. Avis de désengagement de responsabilité</h2>
            <p>Le contenu mis à disposition sur le site est fourni à titre informatif.

                Vous reconnaissez avoir pris connaissance de l'avis de désengagement de responsabilité suivant et consentez à ses modalités. Si vous n'y consentez pas, vous n'êtes pas autorisé(e) à consulter le Site et vous devez fermer les fenêtres de vos navigateurs Internet (Internet Explorer, Chrome, Firefox ou autre) :

                Le contenu, les avis, commentaires et autres renseignements diffusés sur le Site ont pour seul but de fournir de l'information aux visiteurs du Site et ne constituent qu'une partie de l'information à considérer dans une situation de personne effectuant une démarche administrative.

                De plus, VAXHUB se désengage de toute responsabilité de tout dommage direct, indirect, accessoire, connexe ou punitif en raison de l'accès aux renseignements du Site, de leur utilisation ou de l'incapacité de les exploiter. De plus, la rédaction du Site ne peut être tenue responsable de quelque manière que ce soit des erreurs ou des omissions éventuelles dans le contenu du Site.

                Bien que la rédaction du Site s'effectue avec prudence et diligence concernant la validité, l'exactitude, l'exhaustivité et la pertinence des renseignements rendus disponibles sur le Site afin de vous informer le plus précisément possible ; VAXHUB n'est tenue que d'une obligation de moyens quand à l'exactitude, l'ehaustivité et la pertinence de ces renseignements et n'assume aucune responsabilité quant aux conséquences que pourrait avoir l'utilisation de ces renseignements.

                Nous nous réservons le droit de modifier le contenu du Site de quelque façon que ce soit, n'importe quand, pour quelque motif que ce soit, et sans préavis, et nous nous dégageons de toute responsabilité éventuellement attribuable aux modifications apportées.

                VAXHUB ne pourra en aucun cas être tenue responsable de la fiabilité de transmission des données, des temps d'accès et de réponse pour consulter, interroger ou transférer des informations, des éventuelles cas de suspension ou d'interruption de l'accès aux réseaux Internet ou aux services téléphoniques, notamment en cas de défaillance du matériel de réception ou de la ligne de l'Utilisateur.
            </p>
        </div>
        <div class="separateur"></div>
        <div class="art6">
            <h2>6. Propriété intellectuelle</h2>
            <p>Tous les éléments, notamment les textes et images mis en ligne sur le Site sont la propriété de VAXHUB.

                En conséquence et en application des dispositions du Code de la propriété intellectuelle, des dispositions législatives et réglementaires de tous pays et des conventions internationales, toute représentation et / ou reproduction, intégrale ou partielle du site, faite sans le consentement préalable et écrit de VAXHUB, est interdite sous peine d'engager sa responsabilité civile et pénale.
            </p>
        </div>
    </main>
</section>

<?php
include('inc/footer.php'); ?>
