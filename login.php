<?php

session_start();


require('inc/pdo.php');
require('inc/fonction.php');
require('inc/request.php');
require('inc/validation.php');

$errors = array();

if(!empty($_POST['lostPwd'])) {
    header('location:forgot_password.php');
}

if(!empty($_POST['submitted'])) {
    // Faille XSS
    $login = cleanXss('login');
    $password = cleanXss('password');
    // Validation
    $sql = "SELECT * FROM users
            WHERE email = :log";
    $query = $pdo->prepare($sql);
    $query->bindValue('log', $login,PDO::PARAM_STR);
    $query->execute();
    $user = $query->fetch();
    if(!empty($user)) {
        if(password_verify($password,$user['password'])) {
            $_SESSION['user'] = array(
                'id'     => $user['id'],
                'email'  => $user['email'],
                'role'   => $user['role'],
                'ip'     => $_SERVER['REMOTE_ADDR']
            );
            header('Location: users.php');
//                die('ok');
        } else {
            $errors['login'] = 'Errors credentials';
        }
    } else {
        $errors['login'] = 'Errors credentials';
    }
}
include('inc/header.php'); ?>
<section id="login">

    <div class="title">
        <h1>Profil Utilisateur</h1>
    </div>

    <div class="wrapForm">
        <div class="imgForm">
<!--            <img src="asset/img/favicon.png" alt="">-->
        </div>

        <div class="form">
            <form action="" method="post" novalidate>

                <div class="formInput">
                    <label for="login">Pseudo ou E-Mail</label>
                    <input type="text" name="login" id="login" value="<?php getPostValue('login'); ?>">
                    <span class="error"><?php viewError($errors, 'login'); ?></span>
                </div>

                <div class="formInput">
                    <label for="password">Mot de passe *</label>
                    <input type="password" name="password" id="password">
                </div>

                <div class="formButtons">

                    <div class="formInput">
                        <div class="formButton" >
                            <input type="submit" name="submitted" value="Connexion">
                        </div>
                    </div>

                    <div class="formInput">
                        <div class="formButton" >
                            <input type="submit" name="lostPwd" value="Mot de passe oublié">
                        </div>
                    </div>

                </div>
                
            </form>
        </div>

    </div>

</section>

<?php include('inc/footer.php');
