<?php
session_start();
require('inc/pdo.php');
require ('inc/validation.php');
require ('inc/fonction.php');
$errors = array();

if(!empty($_POST['submitted'])) {
    // Faille XSS
    $email = cleanXss('email');
    $password = cleanXss('password');
    $password2 = cleanXss('password2');

    if (!empty($email)) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors['email'] = 'Email incorrect !';
//            die('Email valide pls !');
        }
    } else {
        $errors['email'] = 'Veuillez renseigner ce champ';
//        die('Veuillez renseigner ce champ');
    }

    if ((!empty($email)AND(empty($errors['email'])))){
        $sql ="SELECT COUNT(id) FROM users WHERE email = :email";
        $query = $pdo->prepare($sql);
        $query->bindValue('email',$email,PDO::PARAM_STR);
        $query->execute();

        $count = $query->fetchColumn();

        if ($count > 0){
            $errors['email']= 'Email déja pris';
//            die('Email déja pris');
        }
    }

    $errors = validText($errors, $password,'password',6,16);
    if(empty($errors['password'])){
        if($password!=$password2){
            $errors['password']='Veuillez renseigner deux fois le meme mot de passe';
            die('Veuillez renseigner deux fois le meme mot de passe');
        }
    }

    if(count($errors) == 0) {
//        die('ok');
        $pwdHash = password_hash($password, PASSWORD_DEFAULT);

        $role = 'user'; // user OR admin

        $token = generateRandomString(10);

        $sql = "INSERT INTO users (email, password, created_at, role, token) 
                                     VALUES (:email, :password, NOW(), :role, :token)";
        $query = $pdo->prepare($sql);
        $query->bindValue('email', $email, PDO::PARAM_STR);
        $query->bindValue('password', $pwdHash, PDO::PARAM_STR);
        $query->bindValue('role', $role, PDO::PARAM_STR);
        $query->bindValue('token', $token, PDO::PARAM_STR);
        $query->execute();

//        die('ok');
        header("Location: users.php");
//        exit();

    }

}

include('inc/header.php'); ?>

    <section id="register">
        <div class="title">
            <h1>Inscription</h1>
        </div>
        <div class="wrapForm">

            <div class="imgForm">
<!--                <img src="divers/the_rock_formulaire.png" alt="">-->
            </div>

            <div class="form">

                <form action="" method="post"  novalidate>

                    <div class="formInput">
                        <label for="email">Adresse e-mail :</label>
                        <input type="email" id="email" name="email" required>
                        <span class="error"><?php viewError($errors, 'email'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="password">Mot de passe :</label>
                        <input type="password" id="password" name="password" required>
                        <span class="error"><?php viewError($errors, 'password'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="password2">Confirmer le mot de passe :</label>
                        <input type="password" id="password2" name="password2" required>
                    </div>

                    <div class="formInput">
                        <div class="formButton" >
                            <input type="submit" name="submitted" value="S'inscrire">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>


<?php

include('inc/footer.php'); ?>