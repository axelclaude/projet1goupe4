<?php
session_start();

require('inc/pdo.php');
require('inc/fonction.php');
require('inc/request.php');
require('inc/validation.php');
$errors=array();

if (!empty($_POST['submit'])){
    $email=cleanXss('email');
    $errors = validEmail($errors, $email, 'email');

    if (empty($errors['email'])){
        $sql= "SELECT email,token FROM users WHERE email= :email";
        $query= $pdo->prepare($sql);
        $query->bindValue('email', $email);
        $query->execute();
        $verifEmail= $query->fetch();
        if (!empty($verifEmail)){
            echo '<a href="forgot_password.php?email='.urlencode($verifEmail['email']).'&token='.urlencode($verifEmail['token']).'">Cliquer ici pour modifier votre mot de passe</a>';
            die();
        }else{
            $errors['email']='Aucune adresse mail n\'existe à ce nom*';
        }
    }
}

include('inc/header.php'); ?>
    <section id="oublie">
        <div class="wrap3">
            <div class="form_oublie">
                <h1>Mot de passe oublié ?</h1>
                <form action="" method="post">
                    <div class="email_oublie oublie">
                        <input class="email_css" type="email" name="email" id="email" placeholder="Entrez votre adresse e-mail">
                        <span><?php viewError($errors, 'email'); ?></span>
                    </div>
                    <div class="submit_oublie oublie">
                        <input class="submit_css" type="submit" name="submit" id="submit" value="valider">
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php
include ('inc/footer.php');
