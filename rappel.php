<?php
session_start();
require('inc/pdo.php');
require('inc/fonction.php');
require('inc/request.php');
require('inc/validation.php');

if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $rappelInfo = getInfoById($id);
}

if (!empty($_POST['submit']) && !empty($rappelInfo)) {
    $name = $_POST['name'];
    $content = $_POST['content'];
    $role = $_POST['role'];
    $rappel_day = $_POST['rappel_day'];

    // Préparation de la requête SQL pour la mise à jour
    $sql = "UPDATE rappel SET name = :name, content = :content, role = :role, rappel_day = :rappel_day, modified_at = NOW() WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue(':id', $id);
    $query->bindValue(':name', $name);
    $query->bindValue(':content', $content);
    $query->bindValue(':role', $role);
    $query->bindValue(':rappel_day', $rappel_day);
    $query->execute();

    header('Location: index.php');
    exit;
}

include('inc/header.php');
?>

<section id="infos">
    <div class="wrap2">
        <?php
        if (!empty($rappelInfo)) {
            // Afficher les informations sur le rappel
            echo '<div class="info_rappel"><h1>' . $rappelInfo['name'] . '</h1>';
            echo '<p>Contenu: ' . $rappelInfo['content'] . '</p>';
            echo '<p>Jour de rappel: ' . $rappelInfo['rappel_day'] . '</p>';
        } else {
            echo '<p>Aucune information disponible pour ce rappel.</p>';
        }
        ?>
    </div>
</section>

<?php
include('inc/footer.php');
?>
