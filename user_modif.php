<?php
session_start();

require('inc/pdo.php');
require('inc/fonction.php');
require('inc/validation.php');
require('inc/request.php');

if(!isLogged()){
    header('location: index.php');
}

$errors = [];

if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $userId = getUserById($id);
    if(empty($userId)) {
        die('404');
    }
} else {
    die('404');
}

$query = "SELECT * FROM users WHERE id = :user_id";
$stmt = $pdo->prepare($query);
$stmt->bindParam(':user_id', $id);
$stmt->execute();
$user = $stmt->fetch();


if(!empty($_POST['submitted'])) {

    $lastname = cleanXss('lastname');
    $firstname = cleanXss('firstname');
    $email = cleanXss('email');
    $password = cleanXss('password');
    $password2 = cleanXss('password2');
    $secu = cleanXss('secu');

    $errors = validText($errors, $lastname,'lastname',2,30);
    $errors = validText($errors, $firstname,'firstname',2,30);

    if (!empty($email)AND($user['email'] != $email)) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors['email'] = 'Email valide pls !';}
    }

    if ((!empty($email)AND(empty($errors['email'])))AND($user['email'] != $email)){
        $sql ="SELECT COUNT(id) FROM users WHERE email = :email";
        $query = $pdo->prepare($sql);
        $query->bindValue('email',$email,PDO::PARAM_STR);
        $query->execute();

        $count = $query->fetchColumn();

        if ($count > 0){
            $errors['email']= 'Email déja pris';
        }
    }
    if($secu == 0){
        $errors['secu']= 'Veuillez renseigner votre numéro de secu';
    } else {
        $secuSansEspaces = str_replace(' ', '', $secu);
        if(strlen($secuSansEspaces) != 15){
            $errors['secu'] = 'Veuillez renseigner votre numéro de secu sans la clé';
        } else {
            $secu = $secuSansEspaces;
        }
    }


    if(!empty($_POST['password'])){
        if(password_verify($password,$user['password'])){
            $errors['password'] = 'Mot de passe déja utilisé';
        }

        $errors = validText($errors, $password, 'password', 6, 16);
        if ($password != $password2) {
            $errors['password'] = 'Veuillez renseigner deux fois le meme mot de passe';
        }
        $pwdHash = password_hash($password, PASSWORD_DEFAULT);
        $token = random_string(130);
    }

    if(count($errors) == 0) {

        if(!empty($_POST['password'])){
            $sql = "UPDATE users 
                    SET last_name   = :last_name, 
                        first_name  = :first_name, 
                        password    = :pwdHash, 
                        email       = :email, 
                        numero_secu = :numero_secu, 
                        modified_at = NOW(), 
                        token       = :token 
                    WHERE id        = :id";
        } else {
            $sql = "UPDATE users 
                    SET last_name   = :last_name, 
                        first_name  = :first_name, 
                        email       = :email, 
                        numero_secu = :numero_secu,  
                        modified_at = NOW() 
                    WHERE id        = :id";
        }

        $query = $pdo->prepare($sql);
        $query->bindValue('last_name',$lastname,PDO::PARAM_STR);
        $query->bindValue('first_name',$firstname,PDO::PARAM_STR);
        $query->bindValue('email',$email,PDO::PARAM_STR);
        $query->bindValue('numero_secu',$secu,PDO::PARAM_INT);
        $query->bindValue('id' , $id, PDO::PARAM_INT);

        if(!empty($_POST['password'])){
            $query->bindValue('pwdHash' , $pwdHash, PDO::PARAM_STR);
            $query->bindValue('token' , $token, PDO::PARAM_STR);
        }

        $query->execute();
        header('location:users.php');
    }
}

include('inc/header.php');?>

    <section id="userModif">

        <div class="title">
            <h1>Profil Utilisateur</h1>
        </div>

        <div class="wrapForm">

            <div class="imgForm">
<!--                <img src="divers/utilisateur.png" alt="">-->
            </div>

            <div class="form">

                <form method="POST" action="" novalidate>

                    <div class="formInput">
                        <label for="lastname">NOM</label>
                        <input type="text" id="lastname" name="lastname" value="<?php echo $userId['last_name']; ?>" required>
                        <span class="error"><?php viewError($errors, 'last_name'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="firstname">PRENOM</label>
                        <input type="text" id="firstname" name="firstname" value="<?php echo $userId['first_name']; ?>" required>
                        <span class="error"><?php viewError($errors, 'first_name'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="email">EMAIL</label>
                        <input type="email" id="email" name="email" value="<?php echo $userId['email']; ?>" required>
                        <span class="error"><?php viewError($errors, 'email'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="secu">NUMERO DE SECURITE SOCIALE</label>
                        <input type="text" id="secu" name="secu" value="<?php echo $userId['numero_secu']; ?>">
                        <span class="error"><?php viewError($errors, 'secu'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="password">PASSWORD</label>
                        <input type="password" id="password" name="password" required>
                        <span class="error"><?php viewError($errors, 'password'); ?></span>
                    </div>

                    <div class="formInput">
                        <label for="password2">CONFIRM PASSWORD</label>
                        <input type="password" id="password2" name="password2" required>
                    </div>

                    <div class="formInput">
                        <div class="formButton">
                            <input type="submit" name="submitted" value="Modifier">
                        </div>
                    </div>

                </form>

            </div>

        </div>

    </section>

<?php include('inc/footer.php');