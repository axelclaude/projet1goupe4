<?php
session_start();
require('vendor/autoload.php');
require ('inc/pdo.php');

require('inc/pdo.php');
require('inc/fonction.php');
require('inc/validation.php');
require('inc/request.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$errors = array();

//debug($_POST);

if(!empty($_POST['submitted'])) {
    $sujet = cleanXss('sujet');
    $email = cleanXss('email');
    $message = cleanXss('message');

//    die('ok bouton');

    if (!empty($email)) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors['email'] = 'Email incorrect !';
//            die('Email valide pls !');
        }
    } else {
        $errors['email'] = 'Veuillez renseigner ce champ';
//        die('Veuillez renseigner ce champ');
    }
    $errors = validText($errors, $sujet,'sujet',0,120);
    $errors = validText($errors, $message,'message',2,2000);

    if(count($errors) == 0) {

        $sendmail = new PHPMailer();
        $sendmail->isSMTP();
        $sendmail->Host = 'smtp.gmail.com'; // Votre serveur SMTP
        $sendmail->SMTPAuth = true;
        $sendmail->Username = 'axelclaude.ci@gmail.com'; // Votre adresse e-mail
        $sendmail->Password = 'mdp'; // Votre mot de passe
        $sendmail->Port = 587; // Le port SMTP à utiliser
        $sendmail->SMTPSecure = 'tls'; // Chiffrement TLS

        $sendmail->setFrom('axelclaude.ci@gmail.com', 'AxelC');
        $sendmail->addAddress('axelclaude@gmail.com'); // Adresse de destination
        $sendmail->isHTML(true);
        $sendmail->Subject = 'Nouvelle inscription';
        $sendmail->Body = 'Un nouvel utilisateur s\'est inscrit.';

        if ($sendmail->send()) {
            echo 'E-mail envoyé !';
            die('ok');
        } else {
            echo 'Erreur lors de l\'envoi de l\'e-mail : ' . $sendmail->ErrorInfo;
            die(' notok');
        }



    }
}
include('inc/header.php');

?>

<section id="contact">
    <div class="wrapForm">
        <div class="imgForm">
<!--            <img src="asset/img/favicon.png" alt="">-->
        </div>
        <div class="form">
            <form action="" method="post" novalidate>
                <div class="formInput">
                    <label for="email">EMAIL</label>
                    <input type="email" id="email" name="email" value="<?php getPostValue('email'); ?>">
                    <span class="error"><?php viewError($errors, 'email'); ?></span>
                </div>

                <div class="formInput">
                    <label for="sujet">SUJET</label>
                    <input type="sujet" id="sujet" name="sujet" value="<?php getPostValue('sujet'); ?>">
                    <span class="error"><?php viewError($errors, 'sujet'); ?></span>
                </div>

                <div class="formInput">
                    <label for="message">MESSAGE</label>
                    <textarea name="message" id="message" cols="30" rows="10" placeholder="Votre message ici"><?php getPostValue('message'); ?></textarea>
                </div>

                <div class="formInput">
                    <div class="formButton" >
                        <input type="submit" name="submitted" value="ENVOYER">
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<?php include('inc/footer.php');