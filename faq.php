<?php
session_start();




require('inc/fonction.php');
require('inc/validation.php');
require('inc/request.php');

include('inc/header.php');

?>
<section id="faq">
    <div class="wrap">
        <h1 class="faq_title">Avez-vous des questions ?</h1>
        <button class="accordion">Comment voir mes vaccins? </button>
        <div class="panel">
            <p>Lorem ipsum...</p>
        </div>
        <button class="accordion">Je ne parviens pas a me connecter</button>
        <div class="panel">
            <p>Verifiez votre mot de passe</p>
        </div>

        <button class="accordion">Peut-on prendre un rendez vous ?</button>
        <div class="panel">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium architecto consectetur cum dolor laudantium, libero magni maxime minus, molestiae nesciunt obcaecati omnis quam, quas quasi sequi sunt totam ullam vitae.</p>
        </div>
        <button class="accordion">J'ai oublié mon mot de passe, comment puis-je faire ?</button>
        <div class="panel">
            <p>Vous pouvez récupérer votre mot de passe à l'aide du lien "Mot de passe oublié" sur la page de connexion</p>
        </div>
    </div>
</section>


<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");


            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
</script>

<?php

include('inc/footer.php'); ?>


