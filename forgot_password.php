<?php


require('inc/pdo.php');
require('inc/fonction.php');
require('inc/request.php');
require('inc/validation.php');
require('vendor/autoload.php');

$errors = array();


if (!empty($_POST['submit']) AND !empty($_GET['token'])) {

    $token = $_GET['token'];
    $password = cleanXss('password');
    $password2 = cleanXss('password2');


    if (!empty($email)) {

        $sql = "SELECT * FROM users WHERE token = :token";
        $query = $pdo->prepare($sql);
        $query->bindValue('token', $token, PDO::PARAM_STR);
        $query->execute();

        $user = $query->fetch(PDO::FETCH_ASSOC);
        $userID = $user['id'];
    }

    $errors = validText($errors, $password, 'password', 6, 16);
    if (empty($errors['password'])) {
        if ($password != $password2) {
            $errors['password'] = 'Veuillez renseigner deux fois le meme mot de passe';
            die('Veuillez renseigner deux fois le meme mot de passe');
        }
    }

    if ((empty($errors)) and (!empty($user['email']))) {
        $token = generateRandomString(10);

        $hashPassword = password_hash($password, PASSWORD_DEFAULT);
        $sql = "UPDATE users 
                SET password = :password, 
                    token = :token 
                WHERE id = :userID";

        $query = $pdo->prepare($sql);
        $query->bindValue(':password', $hashPassword, PDO::PARAM_STR);
        $query->bindValue(':token', $token, PDO::PARAM_STR);
        $query->bindValue(':userID', $userID, PDO::PARAM_STR);
        $query->execute();

        $sendmail = new PHPMailer();
        $sendmail->isSMTP();
        $sendmail->Host = 'smtp.gmail.com'; // Votre serveur SMTP
        $sendmail->SMTPAuth = true;
        $sendmail->Username = 'axelclaude.ci@gmail.com'; // Votre adresse e-mail
        $sendmail->Password = 'mdp'; // Votre mot de passe
        $sendmail->Port = 587; // Le port SMTP à utiliser
        $sendmail->SMTPSecure = 'tls'; // Chiffrement TLS

        $sendmail->setFrom('axelclaude.ci@gmail.com', 'AxelC');
        $sendmail->addAddress('axelclaude@gmail.com'); // Adresse de destination
        $sendmail->isHTML(true);
        $sendmail->Subject = 'Nouvelle inscription';
        $sendmail->Body = 'Un nouvel utilisateur s\'est inscrit.';

    }

}

include('inc/header.php');
?>


<section id="lostPwd">
    <?php if(empty($_POST['submit'])){?>
    <div class="title">
        <h1>Réinitialisation du mot de passe</h1>
    </div>
 <div class="wrapForm">
        <div class="imgForm">
<!--            <img src="asset/img/favicon.png" alt="">-->
        </div>

        <div class="form">
            <form action="" method="post" novalidate>

                <div class="formInput">
                    <label for="password">Mot de passe *</label>
                    <input type="password" name="password" id="password">
                </div>

                <div class="formInput">
                    <label for="password2">Confirmer le mot de passe :</label>
                    <input type="password" id="password2" name="password2" required>
                </div>

                <div class="formInput">
                    <div class="formButton" >
                        <input type="submit" name="submit" value="Changer le mot de passe">
                    </div>
                </div>

            </form>

        </div>

    </div>
    <?php } ELSE {?>

    <div class="title"> La suite se passe dans votre boîte email! </div>
    <?php } ?>
</section>

<?php
include('inc/footer.php');
?>
