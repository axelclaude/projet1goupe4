<?php
session_start();
require('inc/fonction.php');
require('inc/validation.php');
require('inc/request.php');

include('inc/header.php'); ?>

    <section id="mentions">
        <div class="wrap_mentions">
            <h1 class="title_mentions">
                MENTIONS LÉGALES
            </h1>
            <p>En utilisant le site Vaxhub.fr vous acceptez de respecter les conditions générales d'utilisation et de navigation ci-après définies. </p>
            <div class="separateur"></div>
        </div>
        <div class="mentions_main">
            <div class="mentions_title">
                <h2>LE SITE VAXHUB.FR NE PERMET PAS LA RÉALISATION D'UNE CONSULTATION MÉDICALE</h2>
            </div>
            <p class="mentions_desc">
                Les informations et services proposés au sein du site vaccination-info-service.fr ne constituent ni directement, ni indirectement une consultation médicale. En aucun cas, les informations et services proposés ne sont susceptibles de se substituer à une consultation, une visite ou un diagnostic formulé par votre médecin ou peuvent être interprétées comme assurant la promotion de médicaments.

                Vous ne devez pas mettre en œuvre les informations disponibles sur le site Vaccination-info-service.fr pour la formulation d'un diagnostic, la détermination d'un traitement ou la prise et/ou la cessation de la prise de médicament sans consulter préalablement un médecin généraliste ou spécialiste.

                Vous reconnaissez que les informations qui sont mises à votre disposition ne sont ni complètes, ni exhaustives et que ces informations ne traitent pas de l'intégralité des différents symptômes, médicaments et traitements appropriés aux pathologies et différents maux quotidiens qui vous intéressent.

                En outre, vaccination-info-service.fr ne garantit en aucun cas un quelconque résultat à la suite de la mise en application des informations et services proposés.

                Plus particulièrement, les résultats que vous obtenez dans le cadre des différents questionnaires proposés ne sont donnés qu'à titre indicatif, et ne présument pas de la qualité de votre état de santé actuel et/ou futur, ou à l'inverse ne peuvent remettre en cause votre état de santé.

                En conséquence, vous reconnaissez que la responsabilité de Vaccination-info-service.fr ne pourra être recherchée au titre de l'information et des services proposés sur le site, et vous acceptez que l'utilisation de ces informations et services s'effectue sous votre seule et entière responsabilité, contrôle et direction.
            </p>
            <div class="separateur"></div>
            <div class="article1">
                <h2>ARTICLE 1 - L'EDITEUR</h2>
                <p>L’édition et la direction de la publication du Site est assurée par le groupe 4, au sein de
                    Need For School.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article2">
                <h2>ARTICLE 2 - L'HEBERGEUR</h2>
                <p>L'hébergeur du Site est la société Blabla, dont le siège social est situé en France , avec le
                    numéro de téléphone : 123456678 + blabla@gmail.com</p>
            </div>
            <div class="separateur"></div>
            <div class="article3">
                <h2>ARTICLE 3 - ACCES AU SITE</h2>
                <p>Le Site est accessible en tout endroit, 7j/7, 24h/24 sauf cas de force majeure, interruption
                    programmée ou non et pouvant découlant d’une nécessité de maintenance.
                    En cas de modification, interruption ou suspension du Site, l'Editeur ne saurait être tenu responsable</p>
            </div>
            <div class="separateur"></div>
            <div class="article4">
                <h2>ARTICLE 4 - COLLECTE DES DONNEES</h2>
                <p>Le Site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect
                    de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers
                    et aux libertés.
                    En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit
                    d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur
                    exerce ce droit :</p>
                <ul class="list">
                    <span class="icone-fleche">&#8594;</span>
                    <li>via un formulaire de contact</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>via son espace personnel</li>
                </ul>
            </div>
            <div class="separateur"></div>
            <div class="article5">
                <h2>ARTICLE 5 - L'UTILISATION ET LA NAVIGATION SUR LE SITE VAXHUB.FR DOIVENT RESPECTER LES LOIS ET RÈGLEMENTS EN VIGUEUR</h2>
                <p>Vous êtes informé que l'ensemble des lois et règlements en vigueur sont applicables sur Internet. A ce titre, lorsque vous utilisez les services offerts sur le site vaxhub.fr et que vous naviguez sur le site vaxhub.fr, il vous appartient de respecter l'ensemble des réglementations applicables suivantes, et notamment :
                <ul class="list">
                    <span class="icone-fleche">&#8594;</span>
                    <li>Les règles en matière de droits d'auteur et à de propriété industrielle tels que notamment créations multimédias, logiciels, textes, articles, photos, marques déposées, base de données, images de toute nature.</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>Les personnes figurant sur les photographies sont des modèles et leur image n'est utilisée qu'à des fins d'illustration.</li>
                </ul>
                <p>Santé publique France et ses partenaires possèdent l'intégralité des droits de propriété intellectuelle et industrielle relatifs au contenu du site Vaxhub.fr. A ce titre, il vous est rappelé que toutes mentions relatives à l'existence de droits ne pourront être supprimées et que toutes reproductions, totales ou partielles, non autorisées constitueraient des actes de contrefaçon.</p>
            </div>
            <div class="separateur"></div>
            <div class="article6">
                <h2>ARTICLE 6 - GARANTIE</h2>
                <p>De manière générale, Santé publique France ne garantit pas la complétude, l'exhaustivité et l'exactitude du contenu des informations et des services proposés sur le site vaxhub.fr, Santé publique France mettant tous les moyens disponibles en œuvre afin d'offrir aux internautes un contenu de qualité.

                    En toutes hypothèses, la responsabilité de Santé publique France, de l'un de ses partenaires ou de ses préposés ne pourra être recherchée au titre de l'utilisation que vous ferez des informations et des services proposés sur le site et/ou de votre navigation.

                    Vous êtes engagés à respecter l'ensemble des réglementations légales et réglementaires en vigueur, et plus particulièrement celles visées au sein de la présente charte.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article7">
                <h2>ARTICLE 7 - DROIT D'AUTEUR ET REPRISE DU CONTENU MIS EN LIGNE</h2>
                <p>Tous les contenus présents sur le site vaxhub.fr sont couverts par le droit d'auteur.

                    La reproduction d'un contenu doit être assortie de :</p>
                <ul class="list">
                    <span class="icone-fleche">&#8594;</span>
                    <li>l'ajout de la mention : " ... paru sur le site vaxhub.fr". Cette mention pointera grâce à un lien hypertexte directement sur le contenu ;</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>l'ajout en bas de chaque contenu de la mention "Droits réservés"</li>
                </ul>
                <p>En outre, les informations utilisées ne doivent l'être qu'à des fins personnelles, associatives ou professionnelles, toute diffusion ou utilisation à des fins commerciales ou publicitaires étant exclues .</p>
            </div>
            <div class="separateur"></div>
            <div class="article8">
                <h2>ARTICLE 8 - DONNEES COLLECTEES</h2>
                <p>Vous pouvez visiter notre Site sans nous communiquer une quelconque information permettant de vous identifier.
                    En effet, comme la plupart des services en ligne, chaque fois que vous visitez notre Site, nous collectons automatiquement certaines données et informations vous concernant et/ou concernant votre ordinateur sans qu’elles permettent de vous identifier. Lorsque vous accédez à, visitez et/ou utilisez notre Site, nous pouvons donc suivre votre visite et collecter certaines données relatives à votre utilisation des Produits et Services et à votre activité sur le Site comme il est décrit ci-après à la section concernant les « cookies ».
                    Dans certains cas, notamment pour vous inscrire ou solliciter de la documentation, il pourra vous être demandé de communiquer certaines données à caractère personnel telles que vos nom, prénom, adresse électronique, numéro de téléphone.

                    Lorsque vous nous contactez ou lorsque vous demandez à être informé par courriel, courrier ou SMS, nous collectons les Données à Caractère Personnel que vous décidez volontairement de nous communiquer. En outre lors d’échanges téléphonique nous pouvons vous demander des informations à caractère personnel ainsi lors de votre utilisation de nos services.

                    Toutefois, si vous ne souhaitez pas communiquer ces Données à Caractère Personnel, certains des services disponibles sur le Site peuvent ne pas être disponibles.

                    Nous collectons également des Données à Caractère Personnel lorsque le Site vous permet de vous inscrire sur «Espace Patient», il vous sera demandé de nous communiquer certaines Données à Caractère Personnel telles que vos nom, prénom, adresse électronique, adresse postale, numéro de téléphone. Nous pouvons également obtenir des Données à Caractère Personnel via votre navigateur Internet, telles que votre Adresse IP, celle-ci étant un numéro automatiquement assigné à votre ordinateur lorsque vous utilisez Internet.

                    Nous pouvons obtenir des informations à votre sujet auprès d’autres sources. Par exemple, nous pouvons solliciter un fournisseur ou collecteur tiers pour nous fournir des informations supplémentaires sur nos patients existants (l’ajout de données ou « data appending »), notamment des informations renseignées par vous sur le Site.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article9">
                <h2>ARTICLE 9 - RESPONSABILITÉ</h2>
                <p>L’ensemble des informations accessibles via ce site sont fournies en l’état.

                    Vaxhub ne donne aucune garantie, explicite ou implicite, et n’assume aucune responsabilité relative à l’utilisation de ces informations.

                    Vaxhub n’est pas responsable ni de l’exactitude, ni des erreurs, ni des omissions contenues sur ce site.

                    L’utilisateur est seul responsable de l’utilisation de telles informations.

                    Vaxhub se réserve le droit de modifier à tout moment les présentes notamment en actualisant ce site.

                    Vaxhub ne pourra être responsable pour quel que dommage que ce soit tant direct qu’indirect, résultant d’une information contenue sur ce site.

                    L’utilisateur s’engage à ne transmettre sur ce site aucune information pouvant entraîner une responsabilité civile ou pénale et s’engage à ce titre à ne pas divulguer via ce site des informations illégales, contraires à l’ordre public ou diffamatoires.

                    Cette information est destinée aux investisseurs localisés en dehors des États-Unis d’Amérique. Le contenu de ce site est à usage informatif uniquement.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article10">
                <h2>ARTICLE 10 - COOKIES</h2>
                <p>Un cookie est un fichier texte déposé sur l’appareil que vous utilisez pour accéder à Internet (ordinateur, téléphone mobile, tablette…) lorsque vous visitez un site via votre logiciel de navigation. Il contient plusieurs données permettant de vous reconnaître :</p>
                <ul class="list">
                    <span class="icone-fleche">&#8594;</span>
                    <li>le nom du serveur qui l’a déposé ;</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>un identifiant sous forme de numéro unique ;</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>éventuellement une date d’expiration.</li>
                </ul>
                <br>
                <p>Les cookies ne contiennent aucune Données à Caractère Personnel et ne permettent pas d’accéder à votre ordinateur.

                    Le délai de validité du consentement au dépôt des Cookies est de 13 mois. À l’expiration de ce délai, nous solliciterons de nouveau votre consentement.

                    Lors de votre première visite sur notre Site, une bannière d’information s’affichera en bas de page. En poursuivant votre navigation sur le Site ou après avoir cliqué sur « J’accepte », vous consentez à ce que Vaxhub puisse déposer des cookies sur votre appareil (les cookies liés aux opérations relatives à la publicité ciblée, certains cookies de mesure d’audience, les cookies des réseaux sociaux générés notamment par leurs boutons de partage lorsqu’ils collectent des données personnelles).

                    Le bandeau restera affiché tant que vous n’avez pas poursuivi votre navigation, c’est-à-dire tant que vous ne vous êtes pas rendu sur une autre page du Site ou n’avez pas cliqué sur un autre élément du Site. Vous avez la possibilité de modifier à tout moment les paramètres de votre navigateur internet afin d’accepter, paramétrer ou, au contraire, refuser les cookies. Plusieurs options s’offrent à vous : accepter tous les cookies, être averti lorsqu’un cookie est enregistré, ou refuser systématiquement tous les cookies.

                    Chaque navigateur internet propose des modalités de configuration différentes pour gérer les cookies. De manière générale, elles sont décrites dans le menu d’aide de chaque navigateur. Si vous utilisez un type ou une version de navigateur différent de ceux figurant sur la liste ci-dessous, nous vous invitons à consulter le menu « Aide » de votre navigateur : Firefox, Chrome, Internet Explorer, Safari et Opera.

                    Toutefois, si vous décidez de refuser les cookies de sessions ou de fonctionnalités, vous ne pourrez accéder à de nombreuses options proposées par le Site. A ce titre, Vaxhub ne saurait être tenu pour responsable en cas de fonctionnement dégradé du Site lié aux paramétrages que vous aurez choisis.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article11">
                <h2>ARTICLE 11 - DESINSCRIPTION DE L'ENVOI SMS/EMAIL</h2>
                <p>Si vous ne souhaitez plus recevoir de courriel Vaxhub vous informant des évènements, nouveaux Produits ou Services, Produits ou Services analogues, ou contenant d’autres communications commerciales, il vous suffit de cliquer sur le lien « se désinscrire » situé en bas de chaque courriel et suivre la procédure indiquée.

                    Si vous ne souhaitez plus recevoir de SMS, vous pouvez envoyer « STOP » au numéro indiqué si l’option est disponible, à défaut prendre contact avec votre interlocuteur habituel.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article12">
                <h2>ARTICLE 12 - MISE A JOUR DE L'ESPACE PATIENT</h2>
                <p>Vous pouvez vous connecter à « Votre Espace » et/ou « Mon Extranet » lorsque le Site vous le permet. Une fois connecté, vous pourrez modifier vos coordonnées et mettre à jour vos Données à Caractère Personnel. Il vous appartient de protéger vos données d’authentification (notamment vos identifiants et mots de passe) et, comme il est indiqué dans les Conditions Générales, vous êtes responsable de toute activité réalisée avec vos données d’authentification.</p>
            </div>
            <div class="separateur"></div>
            <div class="article13">
                <h2>ARTICLE 13 - DROITS SUR MES DONNEES PERSONNELLES</h2>
                <p>En application du cadre légal et règlementaire applicable en matière de protection des données à caractère personnel et de la vie privée, en particulier le RGPD, de la législation française en vigueur et des recommandations des autorités publiques indépendantes instituées par les États membres de l’Union européenne et chargées du contrôle de ce cadre légal et règlementaire, vous disposez d’un droit d’accès, de rectification, d’opposition, de suppression, de limitation des traitements ainsi qu’un droit à la portabilité de vos Données à Caractère Personnel.

                    Vous disposez également du droit de définir les directives relatives au sort de vos Données à Caractère Personnel après votre décès.

                    Dans ce cadre, vous pouvez contacter Vaxhub à tout moment par courrier en écrivant à l’adresse suivante : Vaxhub – Délégué à la Protection des Données – Need For School.

                    En cas de réclamation, vous pouvez choisir de contacter la CNIL.
                </p>
            </div>
            <div class="separateur"></div>
            <div class="article14">
                <h2>ARTICLE 14 - LEGISLATION SUR LA PROTECTION DES DONNEES </h2>
                <p>La présente Politique en Matière de Protection de la Confidentialité et de Cookies est régie par le droit français et, à compter du 25 mai 2018, par le Règlement Général sur la Protection des Données 2016/679 du 27 avril 2016 (« RGPD »). Veuillez noter que cette politique peut évoluer afin de prendre en compte les évolutions réglementaires.</p>
            </div>
            <div class="separateur"></div>
            <div class="article15">
                <h2>ARTICLE 15 - COPYRIGHT </h2>
                <p>L’ensemble des informations présentes sur ce site peut être téléchargé, reproduit, imprimé sous réserve de :</p>
                <ul class="list">
                    <span class="icone-fleche">&#8594;</span>
                    <li>n’utiliser de telles informations qu’à des fins personnelles et en aucune manière à des fins commerciales ;</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>ne pas modifier de telles informations ;</li>
                    <span class="icone-fleche">&#8594;</span>
                    <li>reproduire sur toutes copies la mention des droits d’auteur (« le copyright ») de Vaxhub.</li>
                </ul>
            </div>
            <div class="separateur"></div>
            <div class="fin-mentions">
                <p>Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du Site,
                    sans autorisation de l’Editeur est prohibée et pourra entraînée des actions et poursuites judiciaires
                    telles que notamment prévues par le Code de la propriété intellectuelle et le Code civil.
                    Pour plus d’informations, se reporter aux CGU du site vaxhub.fr accessible à la rubrique "CGU"
                </p>
            </div>
        </div>
    </section>


<?php

include('inc/footer.php');


